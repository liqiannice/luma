package io.github.isliqian.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wxt.liqian
 * @version 2018/8/3
 * 学校类
 */
@Data
public class College implements Serializable {

    private Integer id;

    private String  name; //名称

    private String location;//所在地

    private String url;//百度百科官网

    private String subject;//隶属

    private String type;//类型

    private String level;//学历层次

    private String feature;//特性

    private String postgraduate;//是否是研究生院

    private String satisfaction;//满意度

}
