package io.github.isliqian;


import io.github.isliqian.bean.College;
import io.github.isliqian.splider.CollegeInfo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

/**
 * @param
 * @return
 * @author wxt.liqian
 * @version 2018/8/3
 */
public class LuMa
{

    private static WebDriver driver = null ;
    private static String chromeDriverDir = "C:\\Users\\DELL\\Downloads\\chromedriver.exe";

    public static void main(String[] args) {
        //1.打开浏览器；
        System.setProperty("webdriver.chrome.driver", chromeDriverDir);
        driver = new ChromeDriver();
        //打开文件网址；
        driver.get("https://baike.baidu.com/item/四川文理学院");
        // 点击历年分数线
        driver.findElement(By.xpath("//nav//a[last()-1]")).click();

        List<WebElement> sectionElements1 = driver.findElements(By.xpath("//dt[@id='totalScoresFilter']//div[@id='totalFilter_course']/a"));
        //根据理科文科
        WebElement sectionElement1 = driver.findElement(By.xpath("//dt[@id='totalScoresFilter']//div[@class='filterBox'][1]//a[@class='filterBtn']"));
        for (WebElement webElement:sectionElements1){
            sectionElement1.click();
            webElement.click();
            //根据省份
            List<WebElement> provinceElements=driver.findElements(By.xpath("//div[@id='totalFilter_province']//a[@class='filterItem']"));
            //System.out.println(provinceElements.size());
            for (WebElement provinceElement:provinceElements){
                WebElement provinceElementBtn = driver.findElement(By.xpath("//dt[@id='totalScoresFilter']//div[@class='filterBox'][last()]//a[@class='filterBtn']"));
                provinceElementBtn.click();
                System.out.println(provinceElement.getText());
                provinceElement.click();
                Document doc = Jsoup.parse(driver.getPageSource());
                Elements scoresElements = doc.getElementById("totalScoresTable").select("tbody").select("tr");
                for (Element element:scoresElements){
                    System.out.println(element.text());
                }
            }
        }
        //根据理科文科
        List<WebElement> sectionElements2 = driver.findElements(By.xpath("//dt[@id='majorScoresFilter']//div[@id='majorFilter_course']/a"));
        //根据理科文科
        WebElement sectionElementBtn2 = driver.findElement(By.xpath("//dt[@id='majorScoresFilter']//div[@class='filterBox'][1]//a[@class='filterBtn']"));
        WebElement yearElementBtn = driver.findElement(By.xpath("//dt[@id='majorScoresFilter']//div[@class='filterBox'][2]/a"));
        WebElement provinceElementBtn = driver.findElement(By.xpath("//dt[@id='majorScoresFilter']//div[@class='filterBox'][3]//a[@class='filterBtn']"));
        for (WebElement sectionElementItem:sectionElements2){
            sectionElementBtn2.click();
            sectionElementItem.click();
            //根据省份

            List<WebElement> yearElements=driver.findElements(By.xpath("//dt[@id='majorScoresFilter']//div[@class='filterBox'][2]//div//a"));
            for (WebElement yearElement:yearElements){
                yearElementBtn.click();
                yearElement.click();
                //根据省份
                List<WebElement> provinceElements=driver.findElements(By.xpath("//dt[@id='majorScoresFilter']//div[@class='filterBox'][3]//a[@class='filterItem']"));
                //System.out.println(provinceElements.size());
                for (WebElement provinceElement:provinceElements){
                    provinceElementBtn.click();
                    System.out.println(provinceElement.getText());
                    provinceElement.click();
                    Document doc = Jsoup.parse(driver.getPageSource());
                    Elements scoresElements = doc.getElementById("majorScoresTable").select("tbody").select("tr");
                    for (Element element:scoresElements){
                        System.out.println(element.text());
                    }
                }
            }


        }
        //List<WebElement> scoreElements = driver
        //driver.quit();
    }

//    public static void main( String[] args )  {
//        //爬取高校基本信息
//        List<College> collegeList = CollegeInfo.spider();
//        System.out.println(collegeList.get(1230).getName());
//
//    }
}
