package io.github.isliqian.splider;

import io.github.isliqian.bean.College;
import io.github.isliqian.util.LumaUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author wxt.liqian
 * @version 2018/8/3
 * 百度百科 https://baike.baidu.com/item/学校名
 * 页面规律 start-0，20，40，60。
 */
public class CollegeInfo {

    /**
     * @param pageNum
     * @return java.lang.String
     * @author wxt.liqian
     * @version 2018/8/3
     * URL规律
     */
    public static String url(int pageNum){
        String url = "https://gaokao.chsi.com.cn/sch/search--ss-on,option-qg,searchType-1,start-"+pageNum+".dhtml";
        return url;
    }
    public static int getMaxPageNum()  {
        int pageMaxNum=0;
        try {
            Connection conn= Jsoup.connect(url(0));//获取请求连接
            //获取请求信息
            Document doc = null;
            doc = conn.get();
            //获取页面总数
            Elements elements =doc.select("li.lip");
            pageMaxNum = Integer.parseInt(elements.get(7).text());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return pageMaxNum;
    }
    public static List<College> spider() {
        List<College> collegeList = new ArrayList<>();
        try {
            int pageMaxNum = getMaxPageNum();

            for (int j=0;j<pageMaxNum;j++){
                System.out.println("开始爬取url:" +url(j*20));
                Connection conn= Jsoup.connect(url(j*20));//获取请求连接
                //获取请求信息
                Document doc = null;
                doc = conn.get();

                //获取高校基本信息
                Elements elements = doc.select("table.ch-table").select("tr");
                for (int i = 1;i < elements.size(); i++) {
                    College college  =new College();
                    //获取每一行的列
                    Elements tds = elements.get(i).select("td");
                    college.setName(LumaUtil.getCollegeName(tds.get(0).text()));
                    college.setLocation(tds.get(1).text());
                    college.setSubject(tds.get(2).text());
                    college.setType(tds.get(3).text());
                    college.setLevel(tds.get(4).text());
                    college.setFeature(tds.get(5).text());
                    college.setPostgraduate(tds.get(6).text());
                    college.setSatisfaction(tds.get(7).text());
                    college.setUrl("https://baike.baidu.com/item/"+college.getName());
                    collegeList.add(college);

                }
                if (j>pageMaxNum)break;
            }

//        for (College c: collegeList) {
//            System.out.println(c.toString());
//        }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return collegeList;
    }
}
