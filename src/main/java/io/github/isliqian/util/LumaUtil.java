package io.github.isliqian.util;

/**
 * @author wxt.liqian
 * @version 2018/8/3
 * luma的工具类
 */
public class LumaUtil{

    private static final String STR1  ="\\(";

    public static String getCollegeName(String collegeName){
        String[] strings = collegeName.split(STR1);
        return strings[0];
    }
}
