package io.github.isliqian;

import com.sun.deploy.net.HttpRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;

/**
 * @author wxt.liqian
 * @version 2018/8/3
 */
public class TestURL {
    public static void main(String[] args) {
        HttpServletRequest request=null;
        String destURLStr= "http://www.baidu.com";
        Enumeration e = request.getHeaderNames();
        while (e.hasMoreElements()) {
            String headerName = (String) e.nextElement();
            Enumeration<String> headerValues = request
                    .getHeaders(headerName);
            while (headerValues.hasMoreElements()) {
                System.out.println(headerName + ":"
                        + headerValues.nextElement());
            }
        }
    }

    public static void echoRequestHeaders(HttpURLConnection httpUrlCon){
        System.out.println("Request Headers:");
        System.out.println(" " + httpUrlCon.getRequestMethod() + " / " + " HTTP/1.1");
        System.out.println(" Host: " + httpUrlCon.getRequestProperty("Host"));
        System.out.println(" Connection: " + httpUrlCon.getRequestProperty("Connection"));
        System.out.println(" Accept: " + httpUrlCon.getRequestProperty("Accept"));
        System.out.println(" User-Agent: " + httpUrlCon.getRequestProperty("User-Agent"));
        System.out.println(" Accept-Encoding: " + httpUrlCon.getRequestProperty("Accept-Encoding"));
        System.out.println(" Accept-Language: " + httpUrlCon.getRequestProperty("Accept-Language"));
        System.out.println(" Cookie: " + httpUrlCon.getRequestProperty("Cookie"));
        System.out.println(" Connection: " + httpUrlCon.getHeaderField("Connection"));//利用另一种读取HTTP头字段
        System.out.println();
    }

    public static void echoResponseHeaders(HttpURLConnection httpUrlCon) throws IOException{
        System.out.println("Response Headers:");
        System.out.println(" " + "HTTP/1.1 " + httpUrlCon.getResponseCode() + " " + httpUrlCon.getResponseMessage());
        System.out.println(" status: " + httpUrlCon.getResponseCode() + " " + httpUrlCon.getResponseMessage());
        System.out.println(" content-encoding: " + httpUrlCon.getContentEncoding());
        System.out.println(" content-length : " + httpUrlCon.getContentLength());
        System.out.println(" content-type: " + httpUrlCon.getContentType());
        System.out.println(" Date: " + httpUrlCon.getDate());
        System.out.println(" ConnectTimeout: " + httpUrlCon.getConnectTimeout());
        System.out.println(" expires: " + httpUrlCon.getExpiration());
        System.out.println(" content-type: " + httpUrlCon.getHeaderField("content-type"));//利用另一种读取HTTP头字段
        System.out.println();
    }

}
